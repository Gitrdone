using System;
using System.Collections.Generic;
using System.Text;

namespace Gitrdone
{
    public class Multidict<KeyType, ValueType> : System.Collections.IEnumerable
    {
        protected Dictionary<KeyType, List<ValueType>> m_dict = new Dictionary<KeyType, List<ValueType>>();

        public Multidict()
        {
        }

        public List<ValueType> Get(KeyType key)
        {
            List<ValueType> items;
            if (m_dict.TryGetValue(key, out items))
            {
                return items;
            }
            return null;
        }

        public bool Add(KeyType key, ValueType value)
        {
            return Add(key, value, false);
        }

        public bool Add(KeyType key, ValueType value, bool bAllowDups)
        {
            List<ValueType> items;
            if (!m_dict.TryGetValue(key, out items))
            {
                items = new List<ValueType>();
                m_dict[key] = items;
            }
            if (!items.Contains(value) || bAllowDups)
            {
                items.Add(value);
                return true;
            }
            return false;
        }

        public System.Collections.IEnumerator GetEnumerator()
        {
            return m_dict.GetEnumerator();
        }
    }
}
