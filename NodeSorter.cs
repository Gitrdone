using System;
using System.Collections;
using System.Text;
using System.Windows.Forms;

namespace Gitrdone
{
    public class NodeSorter : IComparer
    {
        public int Compare(object x, object y)
        {
            TreeNode tx = x as TreeNode;
            TreeNode ty = y as TreeNode;

            // Compare the length of the strings, returning the difference.
            if (tx.Nodes.Count > 0 && ty.Nodes.Count < 1)
                return -1;
            if (tx.Nodes.Count < 1 && ty.Nodes.Count > 0)
                return 1;

            return string.Compare(tx.Text, ty.Text);
        }

    }
}
