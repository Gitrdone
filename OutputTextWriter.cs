
using System;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.ComponentModel;

namespace Gitrdone
{
	
	
	public class OutputTextWriter : System.IO.StringWriter
	{
        delegate void SetTextCallback(char[] buffer, int index, int count);

	    ListBox m_listBox;
	    TextWriter m_realConsole;
		public OutputTextWriter(ListBox b, TextWriter w)
		{
		    m_listBox = b;
		    m_realConsole = w;
		}

        public override void Write(char[] buffer, int index, int count)
        {
            if (!m_listBox.InvokeRequired)
            {
                string s = new string(buffer);
                int start = index > -1 ? index : 0;
                int newcount = count < ((buffer.Length - 1) - start) ? count : ((buffer.Length - 1) - start);
                m_listBox.Items.Add(s.Substring(index, newcount).Trim());
                Application.DoEvents();
            }
            
            m_realConsole.Write(buffer, index, count);
        }
		public override void Write(string value)
		{
		    m_listBox.Items.Add(value);
            m_realConsole.Write(value + System.Environment.NewLine);
		    Application.DoEvents();
		}
		
		public void DebugWrite(string value)
		{
		    m_listBox.Items.Add(value);
		    Application.DoEvents();
		}
	}
}
