using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Git;

namespace Gitrdone
{
    public partial class RepoTreeView : UserControl
    {
        ImageList m_imageList = new ImageList();
        TreeNode m_rootNode;
        protected Dictionary<string, string> m_expandedPaths = new Dictionary<string, string>();
        protected int m_selectedTypes = -1;
        Repo m_currentRepo;
        protected Dictionary<string, Git.File> m_knownFiles = new Dictionary<string,Git.File>();

        public RepoTreeView()
        {
            InitializeComponent();
            m_workspaceTree.BeforeExpand += new TreeViewCancelEventHandler(OnBeforeExpand);
            m_workspaceTree.AfterCollapse += new TreeViewEventHandler(OnAfterCollapse);
            m_workspaceTree.ImageList = m_imageList;
            m_workspaceTree.TreeViewNodeSorter = new Gitrdone.NodeSorter();
            m_workspaceTree.FullRowSelect = true;
            m_workspaceTree.AfterSelect += new TreeViewEventHandler(OnNodeSelected);
            PopulateImageList();
        }

        void OnNodeSelected(object sender, TreeViewEventArgs e)
        {
            EnableModifyButtons(m_workspaceTree.SelectedNode != null);
        }

        protected void EnableModifyButtons(bool bEnable)
        {
            m_addFileButton.Enabled = bEnable;
            m_removeFiles.Enabled = bEnable;
            m_revertFiles.Enabled = bEnable;
        }

        void OnAfterCollapse(object sender, TreeViewEventArgs e)
        {
            if (Enabled)
            {
                m_expandedPaths.Remove((string)e.Node.Tag);
            }
        }

        protected void PopulateImageList()
        {
            System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
            System.Resources.ResourceManager m = new System.Resources.ResourceManager("Gitrdone.Properties.Resources", a);
            Image im = (System.Drawing.Image)m.GetObject("folder");
            m_imageList.Images.Add("folder", im);

            im = (System.Drawing.Image)m.GetObject("page");
            m_imageList.Images.Add("normal", im);
            im = (System.Drawing.Image)m.GetObject("page_edit");
            m_imageList.Images.Add("modified", im);
            im = (System.Drawing.Image)m.GetObject("page_add");
            m_imageList.Images.Add("added", im);
            im = (System.Drawing.Image)m.GetObject("page_delete");
            m_imageList.Images.Add("delete", im);
            im = (System.Drawing.Image)m.GetObject("page_error");
            m_imageList.Images.Add("unmerged", im);
            im = (System.Drawing.Image)m.GetObject("disconnect");
            m_imageList.Images.Add("unknown", im);

            im = (System.Drawing.Image)m.GetObject("page_error");
            m_imageList.Images.Add("broken", im);
            im = (System.Drawing.Image)m.GetObject("page_lightning");
            m_imageList.Images.Add("modechanged", im);
            im = (System.Drawing.Image)m.GetObject("page_copy");
            m_imageList.Images.Add("copied", im);
            im = (System.Drawing.Image)m.GetObject("page_go");
            m_imageList.Images.Add("renamed", im);

            m_imageList.Images.Add("empty", new Bitmap(16, 16));
        }

        protected void SetNodeImage(TreeNode n, Git.File f)
        {
            switch (f.State)
            {
                case FileState.Normal:
                    n.ImageKey = "normal";
                    n.SelectedImageKey = "normal";
                    break;
                case FileState.Deleted:
                    n.ImageKey = "delete";
                    n.SelectedImageKey = "delete";
                    break;
                case FileState.Modified:
                    n.ImageKey = "modified";
                    n.SelectedImageKey = "modified";
                    break;
                case FileState.Unknown:
                    n.ImageKey = "unknown";
                    n.SelectedImageKey = "unknown";
                    break;
                case FileState.Unmerged:
                    n.ImageKey = "unmerged";
                    n.SelectedImageKey = "unmerged";
                    break;
                case FileState.Added:
                    n.ImageKey = "added";
                    n.SelectedImageKey = "added";
                    break;
                case FileState.Copied:
                    n.ImageKey = "copied";
                    n.SelectedImageKey = "copied";
                    break;
                case FileState.ModeChanged:
                    n.ImageKey = "modechanged";
                    n.SelectedImageKey = "modechanged";
                    break;
                case FileState.Broken:
                    n.ImageKey = "broken";
                    n.SelectedImageKey = "broken";
                    break;
                case FileState.Renamed:
                    n.ImageKey = "renamed";
                    n.SelectedImageKey = "renamed";
                    break;
                default:
                    n.ImageKey = "unknown";
                    n.SelectedImageKey = "unknown";
                    break;
            }

        }

        protected void AddNewFileNode(TreeNode n, Git.File f)
        {
            if (((int)f.State & m_selectedTypes) != 0)
            {
                TreeNode subNode = n.Nodes.Add(f.Name, f.Name);
                subNode.Tag = f;
                SetNodeImage(subNode, f);
            }
        }

        protected TreeNode AddNewDirNode(TreeNode n, string subpath, string tag)
        {
            TreeNode subNode = n.Nodes.Add(subpath, subpath);
            subNode.ImageIndex = 0;
            subNode.Tag = tag;
            subNode.Nodes.Add(new TreeNode());
            return subNode;
        }

        void OnBeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            m_workspaceTree.SelectedNode = e.Node;
            string path = (string)e.Node.Tag;
            if (Enabled && !m_expandedPaths.ContainsKey(path))
            {
                m_expandedPaths.Add((string)e.Node.Tag, null);
            }
            if (e.Node.FirstNode.Tag != null)
            {
                return;
            }
            e.Node.Nodes.Clear();
            m_workspaceTree.BeginUpdate();
            DirectoryInfo dir = new DirectoryInfo(m_currentRepo.Path + Repo.DirectorySeparator + path);
            FileInfo[] files = dir.GetFiles();
            DirectoryInfo[] subDirs = dir.GetDirectories();
            foreach (DirectoryInfo subDir in subDirs)
            {
                string fullPath = subDir.Name;
                if (!String.IsNullOrEmpty(path))
                {
                    fullPath = path + Repo.DirectorySeparator + subDir.Name;
                }
                if (path.Length == 0 && fullPath == m_currentRepo.GitDir)
                {
                    continue;
                }
                AddNewDirNode(e.Node, subDir.Name, fullPath);
            }

            foreach (FileInfo file in files)
            {
                string fullPath = file.Name;
                if (!String.IsNullOrEmpty(path))
                {
                    fullPath = path + Repo.DirectorySeparator + file.Name;
                }
                Git.File f;
                if (!m_knownFiles.TryGetValue(fullPath, out f))
                {
                    f = new Git.File("", fullPath);
                    f.State = FileState.Normal;
                }
                AddNewFileNode(e.Node, f);
            }
            if (e.Node.Nodes.Count == 0)
            {
                TreeNode n = new TreeNode();
                n.ImageKey = "empty";
                n.SelectedImageKey = null;
                e.Node.Nodes.Add(n);
            }
            m_workspaceTree.EndUpdate();
        }

        public bool SetRepoTree(Repo repo)
        {
            m_currentRepo = repo;
            m_workspaceTree.BeginUpdate();
            m_workspaceTree.Nodes.Clear();
            m_rootNode = new TreeNode("root");
            m_rootNode.Tag = "";
            m_rootNode.ImageIndex = 0;
            m_workspaceTree.Nodes.Add(m_rootNode);
            m_rootNode.Nodes.Add(new TreeNode());
            m_knownFiles.Clear();
            List<Git.File> unmanagedFiles = repo.ListFiles(true);
            foreach (Git.File f in unmanagedFiles)
            {
                m_knownFiles.Add(f.RelativePath + Repo.DirectorySeparator + f.Name, f);
            }
            m_currentRepo.DiffIndex(m_knownFiles, "");
            m_workspaceTree.EndUpdate();
            m_treeStrip.Enabled = true;
            return true;
        }

        protected void OnShowNormal(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)sender).Checked)
            {
                m_selectedTypes = m_selectedTypes | (int)Git.FileState.Normal;
            }
            else
            {
                m_selectedTypes = m_selectedTypes & (int)~Git.FileState.Normal;
            }
            OnRefreshTree(null, null);
        }

        protected void OnShowEdited(object sender, EventArgs e)
        {
            if (((ToolStripButton)sender).Checked)
            {
                m_selectedTypes = m_selectedTypes | (int)Git.FileState.Modified;
            }
            else
            {
                m_selectedTypes = m_selectedTypes & (int)~Git.FileState.Modified;
            }
            OnRefreshTree(null, null);
        }

        protected void OnShowDeleted(object sender, EventArgs e)
        {
            if (((ToolStripButton)sender).Checked)
            {
                m_selectedTypes = m_selectedTypes | (int)Git.FileState.Deleted;
            }
            else
            {
                m_selectedTypes = m_selectedTypes & (int)~Git.FileState.Deleted;
            }
            OnRefreshTree(null, null);
        }

        protected void OnShowOther(object sender, EventArgs e)
        {
            if (((ToolStripButton)sender).Checked)
            {
                m_selectedTypes = m_selectedTypes | (int)Git.FileState.Unknown;
            }
            else
            {
                m_selectedTypes = m_selectedTypes & (int)~Git.FileState.Unknown;
            }
            OnRefreshTree(null, null);
        }

        protected void OnShowUnmerged(object sender, EventArgs e)
        {
            if (((ToolStripButton)sender).Checked)
            {
                m_selectedTypes = m_selectedTypes | (int)Git.FileState.Unmerged;
            }
            else
            {
                m_selectedTypes = m_selectedTypes & (int)~Git.FileState.Unmerged;
            }
            OnRefreshTree(null, null);
        }

        protected void ExpandPath(string s)
        {
            string[] parts = s.Split(Repo.DirectorySeparator.ToCharArray());
            TreeNode currentNode = m_rootNode;
            m_rootNode.Expand();
            foreach (string part in parts)
            {
                int idx = currentNode.Nodes.IndexOfKey(part);
                if (idx > -1)
                {
                    currentNode = currentNode.Nodes[idx];
                    currentNode.Expand();
                }
            }
        }

        protected void OnRefreshTree(object sender, EventArgs e)
        {
            if (null != m_workspaceTree.SelectedNode)
            {
                object o = m_workspaceTree.SelectedNode.Tag;
                SetRepoTree(m_currentRepo);
                this.Enabled = false;
                foreach (string s in m_expandedPaths.Keys)
                {
                    ExpandPath(s);
                }
                this.Enabled = true;
            }
        }

        protected void OnAddFiles(object sender, EventArgs e)
        {
            Enabled = false;
            if (m_workspaceTree.SelectedNode.Tag is Git.File)
            {
                Git.File f = (Git.File)m_workspaceTree.SelectedNode.Tag;
                List<string> paths = new List<string>();
                if (f.RelativePath.Length > 0)
                {
                    paths.Add(f.RelativePath + Repo.DirectorySeparator + f.Name);
                }
                else
                {
                    paths.Add(f.Name);
                }
                List<Git.File> files;
                if (m_currentRepo.Add(paths, out files))
                {
                    Git.File updatedFile = files[0];
                    if (0 != ((int)updatedFile.State & m_selectedTypes))
                    {
                        m_workspaceTree.SelectedNode.Tag = files[0];
                        SetNodeImage(m_workspaceTree.SelectedNode, files[0]);
                    }
                }
            }
            Enabled = true;
        }

        protected void OnDeleteFiles(object sender, EventArgs e)
        {
            Enabled = false;
            if (m_workspaceTree.SelectedNode.Tag is Git.File)
            {
                Git.File f = (Git.File)m_workspaceTree.SelectedNode.Tag;
                List<string> paths = new List<string>();
                if (f.RelativePath.Length > 0)
                {
                    paths.Add(f.RelativePath + Repo.DirectorySeparator + f.Name);
                }
                else
                {
                    paths.Add(f.Name);
                }
                List<Git.File> files;
                if (m_currentRepo.Remove(paths, out files))
                {
                    m_workspaceTree.SelectedNode.Tag = files[0];
                    SetNodeImage(m_workspaceTree.SelectedNode, files[0]);
                }
            }
            Enabled = true;
        }

        protected void OnRevertFiles(object sender, EventArgs e)
        {
            if (m_workspaceTree.SelectedNode.Tag is Git.File)
            {
                Git.File f = (Git.File)m_workspaceTree.SelectedNode.Tag;
                List<string> paths = new List<string>();
                if (f.RelativePath.Length > 0)
                {
                    paths.Add(f.RelativePath + Repo.DirectorySeparator + f.Name);
                }
                else
                {
                    paths.Add(f.Name);
                }
                List<Git.File> files;
                if (m_currentRepo.Revert(paths, out files))
                {
                    m_workspaceTree.SelectedNode.Tag = files[0];
                    SetNodeImage(m_workspaceTree.SelectedNode, files[0]);
                }
            }
        }

        protected void OnCopyFiles(object sender, EventArgs e)
        {

        }

        protected void OnMoveFiles(object sender, EventArgs e)
        {

        }

        protected void OnShowAdded(object sender, EventArgs e)
        {

        }

        protected void OnShowCopied(object sender, EventArgs e)
        {

        }

        protected void OnShowRenamed(object sender, EventArgs e)
        {

        }

        protected void OnShowUnmanaged(object sender, EventArgs e)
        {

        }

        public void Clear()
        {
            m_expandedPaths.Clear();
        }
    }
}
