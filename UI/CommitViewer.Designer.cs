namespace Gitrdone
{
    partial class CommitViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_id = new System.Windows.Forms.TextBox();
            this.m_author = new System.Windows.Forms.TextBox();
            this.m_email = new System.Windows.Forms.TextBox();
            this.m_date = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.m_parents = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.m_message = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_id
            // 
            this.m_id.BackColor = System.Drawing.SystemColors.Window;
            this.m_id.Location = new System.Drawing.Point(82, 12);
            this.m_id.Name = "m_id";
            this.m_id.ReadOnly = true;
            this.m_id.Size = new System.Drawing.Size(445, 20);
            this.m_id.TabIndex = 0;
            // 
            // m_author
            // 
            this.m_author.BackColor = System.Drawing.SystemColors.Window;
            this.m_author.Location = new System.Drawing.Point(82, 41);
            this.m_author.Name = "m_author";
            this.m_author.ReadOnly = true;
            this.m_author.Size = new System.Drawing.Size(445, 20);
            this.m_author.TabIndex = 1;
            // 
            // m_email
            // 
            this.m_email.BackColor = System.Drawing.SystemColors.Window;
            this.m_email.Location = new System.Drawing.Point(82, 70);
            this.m_email.Name = "m_email";
            this.m_email.ReadOnly = true;
            this.m_email.Size = new System.Drawing.Size(445, 20);
            this.m_email.TabIndex = 2;
            // 
            // m_date
            // 
            this.m_date.BackColor = System.Drawing.SystemColors.Window;
            this.m_date.Location = new System.Drawing.Point(82, 99);
            this.m_date.Name = "m_date";
            this.m_date.ReadOnly = true;
            this.m_date.Size = new System.Drawing.Size(445, 20);
            this.m_date.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Commit:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Author:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Email:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Date:";
            // 
            // m_parents
            // 
            this.m_parents.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_parents.FormattingEnabled = true;
            this.m_parents.Location = new System.Drawing.Point(82, 133);
            this.m_parents.Name = "m_parents";
            this.m_parents.Size = new System.Drawing.Size(364, 21);
            this.m_parents.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Parents:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(453, 131);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(74, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "View";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnShowParent);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 188);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Message:";
            // 
            // m_message
            // 
            this.m_message.BackColor = System.Drawing.SystemColors.Window;
            this.m_message.Location = new System.Drawing.Point(12, 221);
            this.m_message.Multiline = true;
            this.m_message.Name = "m_message";
            this.m_message.ReadOnly = true;
            this.m_message.Size = new System.Drawing.Size(515, 286);
            this.m_message.TabIndex = 12;
            this.m_message.WordWrap = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(451, 513);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 13;
            this.button2.Text = "Done";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.OnDone);
            // 
            // CommitViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 548);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.m_message);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.m_parents);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_date);
            this.Controls.Add(this.m_email);
            this.Controls.Add(this.m_author);
            this.Controls.Add(this.m_id);
            this.Name = "CommitViewer";
            this.Text = "CommitViewer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox m_id;
        private System.Windows.Forms.TextBox m_author;
        private System.Windows.Forms.TextBox m_email;
        private System.Windows.Forms.TextBox m_date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox m_parents;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox m_message;
        private System.Windows.Forms.Button button2;


    }
}