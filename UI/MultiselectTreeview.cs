using System;
using System.Collections;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Gitrdone
{
    public class MultiselectTreeview : TreeView
    {
        protected ArrayList m_coll;
        protected TreeNode m_firstNode;

        public ArrayList SelectedNodes
        {
            get
            {
                return m_coll;
            }
            set
            {
                removePaintFromNodes();
                m_coll.Clear();
                m_coll = value;
                paintSelectedNodes();
            }
        }

        protected override void OnAfterSelect(TreeViewEventArgs e)
        {
            // e.Node is the current node exposed by the base TreeView control

            base.OnAfterSelect(e);
            removePaintFromNodes();

            bool bControl = (ModifierKeys == Keys.Control);
            bool bShift = (ModifierKeys == Keys.Shift);

            if (bControl)
            {
                if (!m_coll.Contains(e.Node)) // new node ?
                {
                    m_coll.Add(e.Node);
                }
                else  // not new, remove it from the collection
                {
                    m_coll.Remove(e.Node);
                }
            }
            else
            {
                /*if (bShift)
                {
                    Queue myQueue = new Queue();

                    TreeNode uppernode = m_firstNode;
                    TreeNode bottomnode = e.Node;

                    // case 1 : begin and end nodes are parent
                    bool bParent = isParent(m_firstNode, e.Node);
                    if (!bParent)
                    {
                        bParent = isParent(bottomnode, uppernode);
                        if (bParent) // swap nodes
                        {
                            TreeNode t = uppernode;
                            uppernode = bottomnode;
                            bottomnode = t;
                        }
                    }
                    if (bParent)
                    {
                        TreeNode n = bottomnode;
                        while (n != uppernode.Parent)
                        {
                            if (!m_coll.Contains(n)) // new node ?
                                myQueue.Enqueue(n);

                            n = n.Parent;
                        }
                    }
                    // case 2 : nor the begin nor the
                    // end node are descendant one another
                    else
                    {
                        // are they siblings ?                 

                        if ((uppernode.Parent == null && bottomnode.Parent == null)
                              || (uppernode.Parent != null &&
                              uppernode.Parent.Nodes.Contains(bottomnode)))
                        {
                            int nIndexUpper = uppernode.Index;
                            int nIndexBottom = bottomnode.Index;
                            if (nIndexBottom < nIndexUpper) // reversed?
                            {
                                TreeNode t = uppernode;
                                uppernode = bottomnode;
                                bottomnode = t;
                                nIndexUpper = uppernode.Index;
                                nIndexBottom = bottomnode.Index;
                            }

                            TreeNode n = uppernode;
                            while (nIndexUpper <= nIndexBottom)
                            {
                                if (!m_coll.Contains(n)) // new node ?
                                    myQueue.Enqueue(n);

                                n = n.NextNode;

                                nIndexUpper++;
                            } // end while

                        }
                        else
                        {
                            if (!m_coll.Contains(uppernode))
                                myQueue.Enqueue(uppernode);
                            if (!m_coll.Contains(bottomnode))
                                myQueue.Enqueue(bottomnode);
                        }

                    }

                    m_coll.AddRange(myQueue);

                    paintSelectedNodes();
                    // let us chain several SHIFTs if we like it
                    m_firstNode = e.Node;

                } // end if m_bShift
                else */
                {
                    // in the case of a simple click, just add this item
                    if (m_coll != null && m_coll.Count > 0)
                    {
                        m_coll.Clear();
                    }
                    m_coll.Add(e.Node);
                }
            }
            removePaintFromNodes();
        }

        protected bool isParent(TreeNode parentNode, TreeNode childNode)
        {
            if (parentNode == childNode)
                return true;

            TreeNode n = childNode;
            bool bFound = false;
            while (!bFound && n != null)
            {
                n = n.Parent;
                bFound = (n == parentNode);
            }
            return bFound;
        }


        protected void paintSelectedNodes()
        {
            foreach (TreeNode n in m_coll)
            {
                n.BackColor = SystemColors.Highlight;
                n.ForeColor = SystemColors.HighlightText;
            }
        }

        protected void removePaintFromNodes()
        {
            if (m_coll.Count == 0) return;

            TreeNode n0 = (TreeNode)m_coll[0];
            Color back = n0.TreeView.BackColor;
            Color fore = n0.TreeView.ForeColor;

            foreach (TreeNode n in m_coll)
            {
                n.BackColor = back;
                n.ForeColor = fore;
            }
        }
    }
}