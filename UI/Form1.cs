using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using Git;

namespace Gitrdone
{ 
    public partial class Form1 : Form
    {
        protected OutputTextWriter m_outputWriter;
        Prefs m_prefs = new Prefs();
        Repo m_currentRepo;
        protected Commit m_lastCommitInPage;
        protected Stack<Commit> m_topCommits = new Stack<Commit>();

        public Form1()
        {
            InitializeComponent();
            //TextWriter stdout = System.Console.Out;
            //OutputTextWriter m_outputWriter = new OutputTextWriter(m_outputArea, stdout);
            //System.Console.SetOut(m_outputWriter);            
            
            Executioner.BinaryDirPath = m_prefs.GitDir;
            SetReposList();    
        }

        bool SetReposList()
        {
            if (m_prefs.Repos == null)
            {
                m_prefs.Repos = new System.Collections.Specialized.StringCollection();
            }
            object o = m_reposCombo.SelectedItem;
            m_reposCombo.Items.Clear();
            foreach (string s in m_prefs.Repos)
            {
                m_reposCombo.Items.Add(s);
            }
            m_reposCombo.SelectedItem = o;
            return true;
        }

        void PreferencesUpdated()
        {
            Executioner.BinaryDirPath = m_prefs.GitDir;
            SetReposList();
        }

        protected bool UpdateTags()
        {
            throw new NotImplementedException();
        }

        protected bool UpdateBranches()
        {
            m_branchesCombo.Items.Clear();
            foreach (Branch b in m_currentRepo.Branches)
            {
                m_branchesCombo.Items.Add(b.Name);
            }
            m_branchesCombo.SelectedItem = m_currentRepo.CurrentBranch.Name;
            return true;
        }

        private void OnPreferencesClicked(object sender, EventArgs e)
        {
            PreferencesEditor p = new PreferencesEditor(m_prefs);
            p.ShowDialog();
            PreferencesUpdated();
        }

        private void OnManageRepos(object sender, EventArgs e)
        {
            RepoManager m = new RepoManager(m_prefs);
            m.ShowDialog();
            SetReposList();
        }

        private void OnRepoChanged(object sender, EventArgs e)
        {
            if (m_reposCombo.SelectedIndex > -1)
            {
                m_currentRepo = new Repo(m_reposCombo.SelectedItem.ToString());
                if (!(m_repoViewer.SetRepoTree(m_currentRepo) &&
                        UpdateBranches() /*&& UpdateTags()*/) )
                {
                    MessageBox.Show("There was an error switching to the repo.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                m_fetchCommits.Enabled = true;
                m_commitsView.Items.Clear();
                m_previousCommits.Enabled = false;
                m_nextCommits.Enabled = false;
                m_topCommits.Clear();
                m_lastCommitInPage = null;
            }
            else
            {
                MessageBox.Show("Select a repo.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OnBranchChanged(object sender, EventArgs e)
        {
            if (m_branchesCombo.SelectedIndex > -1)
            {
                if (m_currentRepo.CurrentBranch.Name != m_branchesCombo.SelectedItem.ToString())
                {
                    foreach (Branch b in m_currentRepo.Branches)
                    {
                        if (b.Name == m_branchesCombo.SelectedItem.ToString())
                        {
                            m_currentRepo.Checkout(b);
                        }
                    }
                    m_repoViewer.SetRepoTree(m_currentRepo);
                }
            }
            else
            {
                MessageBox.Show("Select a branch.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void OnCreateBranch(object sender, EventArgs e)
        {

        }

        private void OnMergeBranch(object sender, EventArgs e)
        {

        }

        private void OnDeleteBranch(object sender, EventArgs e)
        {

        }

        private void OnExit(object sender, EventArgs e)
        {

        }

        private void OnResetHard(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Are you sure you want to revert everything you have in your working directory?",
                                                "Gitrdone", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r == DialogResult.Yes)
            {
                m_currentRepo.Reset(null);
                m_repoViewer.SetRepoTree(m_currentRepo);
            }
        }

        private void OnFetchCommits(object sender, EventArgs e)
        {
            List<Commit> commits;
            if (!m_currentRepo.GetLog(null, 100, out commits))
            {
                MessageBox.Show("Could not get commits.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            m_topCommits.Clear();
            m_topCommits.Push(commits[0]);
            FillCommitList(commits);
        }

        private void FillCommitList(List<Commit> commits)
        {
            m_commitsView.BeginUpdate();
            m_commitsView.Items.Clear();
            foreach (Commit c in commits)
            {
                ListViewItem i = new ListViewItem(new string[] { c.ID, c.AuthorName, c.AuthorEmail, c.Date, c.Subject });
                i.Tag = c;
                m_commitsView.Items.Add(i);
            }
            if (commits.Count > 0)
            {
                m_lastCommitInPage = commits[commits.Count - 1];
            }
            m_previousCommits.Enabled = m_nextCommits.Enabled = commits.Count != 0;
            if (m_commitsView.Items.Count != 100)
            {
                m_nextCommits.Enabled = false;
            }
            if (m_topCommits.Count < 2)
            {
                m_previousCommits.Enabled = false;
            }

            m_commitsView.EndUpdate();
        }

        private void OnPreviousCommits(object sender, EventArgs e)
        {
            List<Commit> commits;
            Commit c = m_topCommits.Pop();
            if (!m_currentRepo.GetLog(m_topCommits.Peek(), 100, out commits))
            {
                m_nextCommits.Enabled = false;
                return;
            }
            if (commits.Count < 1)
            {
                m_previousCommits.Enabled = false;
            }
            else
            {
                FillCommitList(commits);
            }
        }

        private void OnNextCommits(object sender, EventArgs e)
        {
            List<Commit> commits;
            if (!m_currentRepo.GetLog(m_lastCommitInPage, 100, out commits))
            {
                m_nextCommits.Enabled = false;
                return;
            }
            if (commits.Count < 1)
            {
                m_nextCommits.Enabled = false;
            }
            else
            {
                m_topCommits.Push(commits[0]);
                FillCommitList(commits);
            }
        }

        private void OnShowCommit(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection items = m_commitsView.SelectedItems;
            if (items.Count == 1)
            {
                CommitViewer c = new CommitViewer((Commit)items[0].Tag);
                c.Show();
            }
        }
    }
}
