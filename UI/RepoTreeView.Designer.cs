namespace Gitrdone
{
    partial class RepoTreeView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_treeStrip = new System.Windows.Forms.ToolStrip();
            this.m_addFileButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton17 = new System.Windows.Forms.ToolStripButton();
            this.m_revertFiles = new System.Windows.Forms.ToolStripButton();
            this.m_removeFiles = new System.Windows.Forms.ToolStripButton();
            this.m_typeSelector = new System.Windows.Forms.ToolStripDropDownButton();
            this.m_normalSelectButton = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.m_refreshButton = new System.Windows.Forms.ToolStripButton();
            this.m_workspaceTree = new System.Windows.Forms.TreeView();
            this.m_treeStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_treeStrip
            // 
            this.m_treeStrip.Enabled = false;
            this.m_treeStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_addFileButton,
            this.toolStripButton17,
            this.m_revertFiles,
            this.m_removeFiles,
            this.m_typeSelector,
            this.m_refreshButton});
            this.m_treeStrip.Location = new System.Drawing.Point(0, 0);
            this.m_treeStrip.Name = "m_treeStrip";
            this.m_treeStrip.Size = new System.Drawing.Size(388, 25);
            this.m_treeStrip.TabIndex = 3;
            this.m_treeStrip.Text = "toolStrip2";
            // 
            // m_addFileButton
            // 
            this.m_addFileButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_addFileButton.Enabled = false;
            this.m_addFileButton.Image = global::Gitrdone.Properties.Resources.add;
            this.m_addFileButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_addFileButton.Name = "m_addFileButton";
            this.m_addFileButton.Size = new System.Drawing.Size(23, 22);
            this.m_addFileButton.Text = "Add/Update Selected Files";
            // 
            // toolStripButton17
            // 
            this.toolStripButton17.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton17.Enabled = false;
            this.toolStripButton17.Image = global::Gitrdone.Properties.Resources.page_copy;
            this.toolStripButton17.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton17.Name = "toolStripButton17";
            this.toolStripButton17.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton17.Text = "Copy Files";
            // 
            // m_revertFiles
            // 
            this.m_revertFiles.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_revertFiles.Enabled = false;
            this.m_revertFiles.Image = global::Gitrdone.Properties.Resources.arrow_redo;
            this.m_revertFiles.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_revertFiles.Name = "m_revertFiles";
            this.m_revertFiles.Size = new System.Drawing.Size(23, 22);
            this.m_revertFiles.Text = "Revert Selected Files";
            // 
            // m_removeFiles
            // 
            this.m_removeFiles.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_removeFiles.Enabled = false;
            this.m_removeFiles.Image = global::Gitrdone.Properties.Resources.delete;
            this.m_removeFiles.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_removeFiles.Name = "m_removeFiles";
            this.m_removeFiles.Size = new System.Drawing.Size(23, 22);
            this.m_removeFiles.Text = "Remove Selected Files";
            // 
            // m_typeSelector
            // 
            this.m_typeSelector.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.m_typeSelector.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_typeSelector.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_normalSelectButton,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6,
            this.toolStripMenuItem8,
            this.toolStripMenuItem7});
            this.m_typeSelector.Image = global::Gitrdone.Properties.Resources.page;
            this.m_typeSelector.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_typeSelector.Name = "m_typeSelector";
            this.m_typeSelector.Size = new System.Drawing.Size(29, 22);
            this.m_typeSelector.Text = "toolStripDropDownButton1";
            // 
            // m_normalSelectButton
            // 
            this.m_normalSelectButton.Checked = true;
            this.m_normalSelectButton.CheckOnClick = true;
            this.m_normalSelectButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_normalSelectButton.Image = global::Gitrdone.Properties.Resources.page;
            this.m_normalSelectButton.Name = "m_normalSelectButton";
            this.m_normalSelectButton.Size = new System.Drawing.Size(171, 22);
            this.m_normalSelectButton.Text = "Up To Date";
            this.m_normalSelectButton.Click += new System.EventHandler(this.OnShowNormal);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Checked = true;
            this.toolStripMenuItem2.CheckOnClick = true;
            this.toolStripMenuItem2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem2.Image = global::Gitrdone.Properties.Resources.page_add;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(171, 22);
            this.toolStripMenuItem2.Text = "Added";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.OnShowAdded);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Checked = true;
            this.toolStripMenuItem3.CheckOnClick = true;
            this.toolStripMenuItem3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem3.Image = global::Gitrdone.Properties.Resources.page_edit;
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(171, 22);
            this.toolStripMenuItem3.Text = "Modified";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.OnShowEdited);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Checked = true;
            this.toolStripMenuItem4.CheckOnClick = true;
            this.toolStripMenuItem4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem4.Image = global::Gitrdone.Properties.Resources.page_error;
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(171, 22);
            this.toolStripMenuItem4.Text = "Unmerged/Broken";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.OnShowUnmerged);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Checked = true;
            this.toolStripMenuItem5.CheckOnClick = true;
            this.toolStripMenuItem5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem5.Image = global::Gitrdone.Properties.Resources.page_copy;
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(171, 22);
            this.toolStripMenuItem5.Text = "Copies Added";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.OnShowCopied);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Checked = true;
            this.toolStripMenuItem6.CheckOnClick = true;
            this.toolStripMenuItem6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem6.Image = global::Gitrdone.Properties.Resources.page_go;
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(171, 22);
            this.toolStripMenuItem6.Text = "Renamed";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.OnShowRenamed);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Checked = true;
            this.toolStripMenuItem8.CheckOnClick = true;
            this.toolStripMenuItem8.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem8.Image = global::Gitrdone.Properties.Resources.page_delete;
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(171, 22);
            this.toolStripMenuItem8.Text = "Deleted";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.OnShowDeleted);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Checked = true;
            this.toolStripMenuItem7.CheckOnClick = true;
            this.toolStripMenuItem7.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem7.Image = global::Gitrdone.Properties.Resources.disconnect;
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(171, 22);
            this.toolStripMenuItem7.Text = "Unmanaged";
            this.toolStripMenuItem7.Click += new System.EventHandler(this.OnShowUnmanaged);
            // 
            // m_refreshButton
            // 
            this.m_refreshButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.m_refreshButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_refreshButton.Image = global::Gitrdone.Properties.Resources.arrow_refresh;
            this.m_refreshButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_refreshButton.Name = "m_refreshButton";
            this.m_refreshButton.Size = new System.Drawing.Size(23, 22);
            this.m_refreshButton.Text = "toolStripButton12";
            this.m_refreshButton.Click += new System.EventHandler(this.OnRefreshTree);
            // 
            // m_workspaceTree
            // 
            this.m_workspaceTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_workspaceTree.Location = new System.Drawing.Point(0, 25);
            this.m_workspaceTree.Name = "m_workspaceTree";
            this.m_workspaceTree.Size = new System.Drawing.Size(388, 519);
            this.m_workspaceTree.TabIndex = 5;
            // 
            // RepoTreeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_workspaceTree);
            this.Controls.Add(this.m_treeStrip);
            this.Name = "RepoTreeView";
            this.Size = new System.Drawing.Size(388, 544);
            this.m_treeStrip.ResumeLayout(false);
            this.m_treeStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip m_treeStrip;
        private System.Windows.Forms.ToolStripButton m_addFileButton;
        private System.Windows.Forms.ToolStripButton toolStripButton17;
        private System.Windows.Forms.ToolStripButton m_revertFiles;
        private System.Windows.Forms.ToolStripButton m_removeFiles;
        private System.Windows.Forms.ToolStripButton m_refreshButton;
        private System.Windows.Forms.TreeView m_workspaceTree;
        private System.Windows.Forms.ToolStripDropDownButton m_typeSelector;
        private System.Windows.Forms.ToolStripMenuItem m_normalSelectButton;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
    }
}
