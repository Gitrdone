using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Git;

namespace Gitrdone
{
    public partial class CommitViewer : Form
    {
        protected Commit m_commit;
        public CommitViewer(Commit c)
        {
            m_commit = c;
            InitializeComponent();
            m_id.Text = c.ID;
            m_author.Text = c.AuthorName;
            m_email.Text = c.AuthorEmail;
            m_date.Text = c.Date;
            m_message.Text = c.Subject + Repo.LineSeparator + Repo.LineSeparator + c.Body;
            m_parents.Items.AddRange(c.Parents.ToArray());
        }

        private void OnShowParent(object sender, EventArgs e)
        {
            Commit c = new Commit(m_commit.Path, m_parents.SelectedItem.ToString());
            CommitViewer v = new CommitViewer(c);
            v.Show();
        }

        private void OnDone(object sender, EventArgs e)
        {
            Close();
        }
    }
}