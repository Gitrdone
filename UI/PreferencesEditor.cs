using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Gitrdone
{
    public partial class PreferencesEditor : Form
    {
        internal Prefs m_prefs;

        internal PreferencesEditor(Prefs p)
        {
            m_prefs = p;
            InitializeComponent();
            m_gitPath.Text = m_prefs.GitDir;
        }

        protected string ChooseDir()
        {
            DialogResult r = m_folderChooser.ShowDialog();
            if (r == DialogResult.OK)
            {
                return m_folderChooser.SelectedPath;
            }
            return "";
        }

        private void OnChooseMinGWPath(object sender, EventArgs e)
        {
            string dir = ChooseDir();
            if (!String.IsNullOrEmpty(dir))
            {
                m_gitPath.Text = dir;
            }
        }

        private void OnSave(object sender, EventArgs e)
        {
            string dir = m_gitPath.Text;
            if (!System.IO.File.Exists(dir + System.IO.Path.DirectorySeparatorChar + "git-init-db") &&
                    !System.IO.File.Exists(dir + System.IO.Path.DirectorySeparatorChar + "git-init-db.exe"))
            {
                MessageBox.Show("The location does not appear to have git executables in it.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            m_prefs.GitDir = dir;
            m_prefs.Save();
            Close();
        }
    }
}