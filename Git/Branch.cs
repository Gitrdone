using System;
using System.Collections.Generic;
using System.Text;

namespace Git
{
    public class Branch : Treeish
    {
        protected string m_name;
        public override string Name { get { return m_name; } internal set { m_name = value; } }
        
        protected string m_hash;
        public override string ID { get { return m_hash; } internal set { m_hash = value; } }
        
        public Branch(string path, string name) : base(path)
        {
            m_name = name;
        }

        public override bool Verify()
        {
            Executioner e = Executioner.GetExecutioner(m_path, false);
            string output, error;
            bool retval = 0 == e.Execute("git-show-ref", "-s --heads " + Name, "", out output, out error);
            m_hash = output.Trim();
            return retval;
        }
    }
}
