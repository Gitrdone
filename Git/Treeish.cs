
using System;

namespace Git
{
    public abstract class Treeish
	{
        public abstract string ID { get; internal set; }
        public abstract string Name { get; internal set; }
	    
	    protected string m_path;
        public string Path { get { return m_path; } }
	    
	    public Treeish(string path)
	    {
	        m_path = path;
	    }
	    
	    public virtual bool Verify()
	    {
	        Executioner e = Executioner.GetExecutioner(m_path, false);
            return 0 == e.Execute("git-rev-parse", "--verify " + ID);
	    }
	}
}
