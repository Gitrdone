
using System;

namespace Git
{
	public enum FileState
	{
        Normal = 1,
	    Added = 1<<1,
	    Copied = 1<<2,
	    Deleted = 1<<3,
	    Modified = 1<<4,
	    Renamed = 1<<5,
        ModeChanged = 1<<6,
        Unmerged = 1<<7,
        Unknown = 1<<8,
        Broken = 1<<9
	}
	
	public class File : IComparable<File>
	{
	    protected string m_mode;
	    public string Mode { get { return m_mode; } set { m_mode = value; } }
	    
	    protected string m_hash;
		public string ID { get { return m_hash; } }
		
		protected string m_relPath; 
		public string RelativePath { get { return m_relPath; } }
		
		protected string m_name; 
		public string Name { get { return m_name; } }
		
		protected int m_stage;
		public int Stage
		{
		    get
		    {
		        return m_stage;
		    }
		    
		    set
		    {
		        if (value < 0 || value > 3)
		        {
		            m_stage = value;
		        }
		        m_stage = value;
		    }
		}
		
		protected FileState m_state = FileState.Unknown;
		public FileState State
		{
		    get
		    {
		        return m_state;
		    }
		    
		    set
		    {
		        m_state = value;
		    }
		}
		
		
		public File(string id, string relativePath)
		{
		    m_hash = id;
            int idx = relativePath.LastIndexOf(Repo.DirectorySeparator);
            if (idx < 0)
            {
                m_relPath = "";
                m_name = relativePath;
            }
            else
            {
                m_relPath = relativePath.Substring(0, idx);
                m_name = relativePath.Substring(idx + 1);
            }
		}
		
		public bool SetStage(string val)
	    {
	        int i;
	        if (!int.TryParse(val, out i))
	        {
	            return false;
	        }
	        if (m_stage < 0 || m_stage > 3)
	        {
	            return false;
	        }
	        m_stage = i;
	        return true;
	    }
	    
	    public int CompareTo(File other)
	    {
	        return m_name.CompareTo(other.m_name);
	    }
	    
        /*************
         * Added (A)
         * Copied (C) Has 2 Files, source, dest
         * Deleted (D)
         * Modified (M)
         * Renamed (R) Has 2 Files, source, dest
         * mode changed (T)
         * Unmerged (U)
         * Unknown (X)
         * Broken (B)
         * ************/
	    public void SetState(string val)
	    {
	        val = val.Trim();
	        switch(val)
	        {
	            case "A":
                   m_state = FileState.Added;
                   break;
                case "C":
                   m_state = FileState.Copied;
                   break;
                case "D":
                   m_state = FileState.Deleted;
                   break;
                case "M":
                   m_state = FileState.Modified;
                   break;
                case "R":
                   m_state = FileState.Renamed;
                   break;
                case "T":
                   m_state = FileState.ModeChanged;
                   break;
                case "U":
                   m_state = FileState.Unmerged;
                   break;
                case "X":
                   m_state = FileState.Unknown;
                   break;
                case "B":
                   m_state = FileState.Broken;
                   break;
               default:
                   m_state = FileState.Unknown;
                   break;
           }
	    }
	}
}
