
using System;
using System.Diagnostics;
using System.Threading;
using System.Text;

namespace Git
{
	
	
	public abstract class Executioner
	{
		protected string m_cwd;
		public string Cwd { get { return m_cwd; } set { m_cwd = value; } } 

        bool SuppressOut { get { return m_bSuppressStdOut; } set { m_bSuppressStdOut = value; } }		
		bool m_bSuppressStdOut = false;
		
		bool SuppressCommand { get { return m_bSuppressCommand; } set { m_bSuppressCommand = value; } }		
		bool m_bSuppressCommand = false;

        static protected string m_binaryDirPath = "";
        static public string BinaryDirPath { get { return m_binaryDirPath; } set { m_binaryDirPath = value; } }
		
	    protected Executioner(string cwd)
	    {
	        m_cwd = cwd;
	    }
	    
	    public static Executioner GetExecutioner(string cwd)
	    {
	        return new UnixExecutioner(cwd);
	    }
	    
	    public static Executioner GetExecutioner(string cwd, bool bPartialSuppress)
	    {
	        Executioner e = new UnixExecutioner(cwd);
	        e.SuppressOut = true;
	        e.SuppressCommand = !bPartialSuppress;
	        return e;
	    }
	    
	    public abstract int Execute(string command, string arguments, string input, out string output, out string error);
	    
	    public int Execute(string command)
	    {
	        string output, error;
	        return Execute(command, "", "", out output, out error);
	    }
	    
	    public int Execute(string command, string arguments)
	    {
	        string output, error;
	        return Execute(command, arguments, "", out output, out error);
	    }
		
		public class UnixExecutioner : Executioner
		{
		    protected StringBuilder m_stdout = new StringBuilder();
		    protected StringBuilder m_stderr = new StringBuilder();
		    
		    public UnixExecutioner(string cwd) : base(cwd)
		    {
		    }
		    
		    public override int Execute(string command, string arguments, string input, out string output, out string error)
		    {
                try
                {
                    Process p = new Process();
                    if (!String.IsNullOrEmpty(m_binaryDirPath))
                    {
                        command = m_binaryDirPath + System.IO.Path.DirectorySeparatorChar + command; 
                    }
                    ProcessStartInfo si = new ProcessStartInfo(command, arguments);
                    si.CreateNoWindow = true;
                    si.ErrorDialog = false;
                    si.UseShellExecute = false;
                    if (m_cwd.Length != 0)
                    {
                        si.WorkingDirectory = m_cwd;
                    }
                    si.RedirectStandardError = true;
                    si.RedirectStandardOutput = true;
                    si.RedirectStandardInput = true;
                    m_stdout.Remove(0, m_stdout.Length);
                    m_stderr.Remove(0, m_stderr.Length);
                    p.StartInfo = si;
                    if (!m_bSuppressCommand)
                    {
                        System.Console.WriteLine("-" + command + " " + arguments + "-");
                    }
                    p.OutputDataReceived += OnReadStdOut;
                    p.ErrorDataReceived += OnReadStdErr;
                    p.Start();
                    p.BeginOutputReadLine();
                    p.BeginErrorReadLine();
                    if (!String.IsNullOrEmpty(input))
                    {
                        p.StandardInput.Write(input);
                        p.StandardInput.Close();
                    }
                    p.WaitForExit();
                    output = m_stdout.ToString();
                    error = m_stderr.ToString();
                    return p.ExitCode;
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message + " " + ex.StackTrace);
                    output = error = "";
                    return -1;
                }
		    }
		    
		    private void OnReadStdOut(object sendingProcess, 
                                                        DataReceivedEventArgs outLine)
		    {
		        if (String.IsNullOrEmpty(outLine.Data))
		        {
		            return;
		        }
		        m_stdout.Append(outLine.Data + Environment.NewLine);
		        if (!m_bSuppressStdOut)
		        {
		            System.Console.WriteLine(outLine.Data);
                }
		    }
		    
		    private void OnReadStdErr(object sendingProcess, 
                                                        DataReceivedEventArgs outLine)
		    {
		        if (String.IsNullOrEmpty(outLine.Data))
		        {
		            return;
		        }
		        m_stderr.Append(outLine.Data + Environment.NewLine);
                System.Console.WriteLine("!!!" + outLine.Data + "!!!");
            }
		}
	}
}
