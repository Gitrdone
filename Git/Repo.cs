using System;
using System.Collections.Generic;
using System.Text;

namespace Git
{
    public class Repo
    {
        Branch m_CurrentBranch = null;
        public Branch CurrentBranch { get { return m_CurrentBranch; } }

        List<Branch> m_branches = new List<Branch>();
        public List<Branch> Branches { get { return new List<Branch>(m_branches); } }

        Tree m_workingSet = null;
        public Tree WorkingSet { get { return m_workingSet; } }

        List<Remote> m_remotes = new List<Remote>();
        public List<Remote> Remotes { get { return new List<Remote>(m_remotes); } }

        List<Tag> m_tags = new List<Tag>();
        public List<Tag> Tags { get { return new List<Tag>(m_tags); } }

        string m_path;
        public string Path { get { return m_path; } }

        public static string DirectorySeparator { get { return "/"; } } //git-ls always uses "/"

        protected static bool m_mono = Type.GetType ("Mono.Runtime") != null;
        public static bool Mono { get { return m_mono; } }

        protected static string[] m_lineSep = { System.Environment.NewLine };
        public static string[] LineSeparator { get { return m_lineSep; } }

        protected string m_gitDir;
        public string GitDir
        {
            get
            {
                if (String.IsNullOrEmpty(m_gitDir))
                {
                    if (!ValidateRepo())
                    {
                        return null;
                    }
                }
                return m_gitDir;
            }
        }

        public Repo(string path)
        {
            m_path = path;
            if (UpdateBranches())
            {
                UpdateTags();
            }
        }
        
        protected bool ValidateRepo()
        {
            return ValidateRepo(m_path, out m_gitDir);
        }

        public static bool ValidateRepo(string path)
        {
            string output;
            return ValidateRepo(path, out output);
        }

        public static bool ValidateRepo(string path, out string dir)
        {
            Executioner e = Executioner.GetExecutioner(path, false);
            string error;
            bool retval = 0 == e.Execute("git-rev-parse", "--git-dir", "", out dir, out error);
            dir = dir.Trim();
            return retval;
        }

        public bool Init()
        {
            if (ValidateRepo())
            {
                return true;
            }
            Executioner e = Executioner.GetExecutioner(m_path);
            return 0 == e.Execute("git-init-db");
        }

        public bool Commit(string message, string[] signedOff, out Commit commited)
        {
            throw new NotImplementedException();
        }

        public bool GetFiles(List<string> paths, out List<File> files)
        {
            if (paths.Count < 1)
            {
                files = new List<Git.File>();
                return true;
            }
            
            paths[0] = "\"" + paths[0];
            paths[paths.Count - 1] = paths[paths.Count - 1] + "\"";
            string pathset = String.Join("\" \"", paths.ToArray());
            string input = "-c -d -m -o -t -z -k " + pathset;
            string output, error;
            Dictionary<string, File> filesMap = new Dictionary<string, File>();
            Executioner e = Executioner.GetExecutioner(m_path);
            if (0 == e.Execute("git-ls-files", input, "", out output, out error))
            {
                ParseLSOutput(output, filesMap);
                DiffIndex(filesMap, pathset);
                files = new List<Git.File>(filesMap.Values);
                return true;
            }
            files = null;
            return false;
        }

        public bool Add(List<string> paths, out List<File> files)
        {
            Executioner e = Executioner.GetExecutioner(m_path);
            files = null;
            string input = String.Join("\n", paths.ToArray());
            string output, error;
            if (e.Execute("git-update-index", "--add --stdin", input, out output, out error) == 0)
            {
                return GetFiles(paths, out files);
            }
            return false;
        }

        protected string GetPathsList(List<string> paths)
        {
            List<string> pathsCopy = new List<string>(paths);
            pathsCopy[0] = " \"" + pathsCopy[0];
            pathsCopy[paths.Count - 1] = pathsCopy[paths.Count - 1] + "\"";
            return String.Join("\" \"", pathsCopy.ToArray());
        }

        public bool Revert(List<string> paths, out List<File> files)
        {
            if (paths.Count < 1)
            {
                files = new List<File>();
                return true;
            }
            Executioner e = Executioner.GetExecutioner(m_path);

            string input = String.Join("\n", paths.ToArray());
            string output, error;
           
            if (e.Execute("git-update-index", "--add --remove --stdin", input, out output, out error) == 0 &&
                e.Execute("git-update-index", "--refresh") == 0)
            {
                return GetFiles(paths, out files);
            }
            files = null;
            return false;
        }

        public bool Remove(List<string> paths, out List<File> files)
        {
            if (paths.Count < 1)
            {
                files = new List<File>();
                return true;
            }
            Executioner e = Executioner.GetExecutioner(m_path);
            
            string input = GetPathsList(paths);
            if (e.Execute("git-rm", "--cached -f " + input) == 0)
            {
                return GetFiles(paths, out files);
            }
            files = null;
            return false;
        }

        public bool Checkout(Treeish t)
        {
            if (!t.Verify())
            {
                return false;
            }
            Executioner e = Executioner.GetExecutioner(m_path);
            return 0 == e.Execute("git-checkout", t.ID);
        }

        public bool Checkout(Tag t, string newBranchName)
        {
            if (!t.Verify())
            {
                return false;
            }
            Executioner e = Executioner.GetExecutioner(m_path);
            if (0 == e.Execute("git-checkout", "-b " + newBranchName + " " + t.Name))
            {
                UpdateBranches();
                return true;
            }
            return false;
        }
        
        protected bool UpdateBranches()
        {
            Executioner e = Executioner.GetExecutioner(m_path, false);
            string output, error;
            if (0 == e.Execute("git-branch", "", "", out output, out error))
            {
                m_branches.Clear();
                foreach (string s in output.Split(m_lineSep, StringSplitOptions.RemoveEmptyEntries))
                {
                    string branchName = s.Trim();
                    if (branchName.StartsWith("*"))
                    {
                        branchName = branchName.Substring(2);
                        branchName = branchName.Trim();
                        m_CurrentBranch = new Branch(m_path, branchName);
                        m_branches.Add(m_CurrentBranch);
                    }
                    else
                    {
                        m_branches.Add(new Branch(m_path, branchName));
                    }
                }
                return true;
            }
            return false;
        }

        protected bool UpdateTags()
        {
            Executioner e = Executioner.GetExecutioner(m_path, false);
            string output = null, error = null;
            if (0 == e.Execute("git-rev-parse", "--symbolic --tags", "", out output, out error))
            {
                m_tags.Clear();
                foreach (string s in output.Split(m_lineSep, StringSplitOptions.RemoveEmptyEntries))
                {
                    m_tags.Add(new Tag(s.Trim(), m_path));
                }
                return true;
            }
            return false;
        }
        
        public bool NewBranch(string name, Branch startPoint, Remote remote, bool bForce)
        {
            Executioner e = Executioner.GetExecutioner(m_path);
            string arguments = name;
            if (bForce)
            {
                arguments = arguments + " -f";
            }
            if (null != startPoint)
            {
                arguments = arguments + " " + startPoint.Name;
            }
            if (null != remote)
            {
                arguments = arguments + " --track " + remote.Name;
            }
            bool retval = 0 == e.Execute("git-branch", arguments);
            UpdateBranches();
            return retval;
        }

        public bool RenameBranch(Branch b, string newName, bool bForce)
        {
            Executioner e = Executioner.GetExecutioner(m_path);
            bool retval = 0 == e.Execute("git-branch", bForce ? "-M " : "-m " + b.Name + " " + newName);
            UpdateBranches();
            return retval;
        }

        public bool DeleteBranch(Branch b, bool bForce)
        {
            Executioner e = Executioner.GetExecutioner(m_path);
            bool retval =  0 == e.Execute("git-branch", bForce ? "-D " : "-d " + b.Name);
            UpdateBranches();
            return retval;
        }

        public List<File> ListFiles(bool bJustUnmanaged)
        {
            Executioner e = Executioner.GetExecutioner(m_path, true);
            string output, error;
            Dictionary<string, File> files = new Dictionary<string, File>();
            string args = bJustUnmanaged ? "-t -o -z" : "-c -d -m -o -k -t -z";
            if (0 == e.Execute("git-ls-files", args, "", out output, out error))
            {
                ParseLSOutput(output, files);
            }
            return new List<File>(files.Values);
        }

        public void DiffIndex(Dictionary<string, File> filesMap, string paths)
        {
            Executioner e = Executioner.GetExecutioner(m_path, true);
            string output, error;
            if (0 == e.Execute("git-diff-index", "-B -M -C -z --name-status HEAD " + paths, "", out output, out error))
            {
                string[] lines = output.Split("\0".ToCharArray());
                for (int i = 0; i < lines.Length - 1; i += 2)
                {
                    string front = lines[i][0].ToString();
                    string file = lines[i + 1].Trim();
                    if (file.StartsWith("\""))
                    {
                        file = file.Substring(1);
                    }
                    if (file.EndsWith("\""))
                    {
                        file = file.Substring(0, file.Length - 1);
                    }
                    Git.File f;
                    f = UpdateMap(filesMap, front, file);
                    if (front == "R")
                    {
                        string file2 = lines[i + 2];
                        UpdateMap(filesMap, "R", file2);
                        ++i;
                    }
                    if (front == "C")
                    {
                        string file2 = lines[i + 2];
                        UpdateMap(filesMap, "A", file2);
                        ++i;
                    }
                }
            }
        }

        private static Git.File UpdateMap(Dictionary<string, File> filesMap, string type, string file)
        {
            Git.File f;
            if (!filesMap.TryGetValue(file, out f))
            {
                f = new Git.File("", file);
                filesMap.Add(file, f);
            }
            f.SetState(type);
            return f;
        }

        private void ParseLSOutput(string output, Dictionary<string, File> files)
        {
            string[] lines = output.Split("\0".ToCharArray());
            System.Console.WriteLine("path: " + m_path + " files: " + lines.Length);
            foreach (string oneline in lines)
            {
                string line = oneline.Trim();
                if (line.Length < 3)
                {
                    continue;
                }
                
                string front = line[0].ToString();
                line = line.Substring(2);
                if (line.StartsWith("\""))
                {
                    line = line.Substring(1);
                }
                if (line.EndsWith("\""))
                {
                    line = line.Substring(0, line.Length - 1);
                }
                File f;
                if (!files.TryGetValue(line, out f))//work around git reporting files twice sometimes
                {
                    f = new File(null, line);
                    files.Add(line, f);
                }
                if (front == "?")
                {
                    f.State = FileState.Unknown;
                }
                else
                {
                    f.State = FileState.Normal;
                }
            }
        }

        public bool Reset(string commit)
        {
            Executioner e = Executioner.GetExecutioner(m_path);
            string id = null == commit ? "HEAD" : commit;
            return (0 == e.Execute("git-read-tree", "--reset -u " + id));
        }

        public bool GetLog(Commit start, int count, out List<Commit> commits)
        {
            Executioner e = Executioner.GetExecutioner(m_path, true);
            string args = "-n " + count + Git.Commit.FormatString;
            if (null != start)
            {
                args += start.ID;
            }
            string output, error;
            if (0 == e.Execute("git-log", args, "", out output, out error))
            {
                string[] records = output.Split(Repo.LineSeparator, StringSplitOptions.RemoveEmptyEntries);
                commits = new List<Commit>();
                foreach (string record in records)
                {
                    Commit c = Git.Commit.GetCommit(m_path, record);
                    commits.Add(c);
                }
                return true;
            }
            commits = null;
            return false;
        }
    }
}
