using System;
using System.Collections.Generic;
using System.Text;

namespace Git
{
    public class Commit : Treeish
    {
        protected List<string> m_Parents;
        public List<string> Parents
        {
            get
            {
                if (null == m_Parents && !Populate())
                {
                    return null;
                }
                return new List<string>(m_Parents);
            }
            internal set { m_Parents = value; }
        }

        public override string Name { get { return "Commit"; } internal set { } }
        
        protected string m_hash;
        public override string ID { get { return m_hash; } internal set { m_hash = value; } }

        protected string m_author;
        public string AuthorName
        {
            get
            {
                if (null == m_author && !Populate())
                {
                    return null;
                }
                return m_author;
            }
            internal set { m_author = value; }
        }

        protected string m_authorEmail;
        public string AuthorEmail
        {
            get
            {
                if (null == m_authorEmail && !Populate())
                {
                    return null;
                }
                return m_authorEmail;
            }
            internal set { m_authorEmail = value; }
        }

        protected string m_date;
        public string Date
        {
            get
            {
                if (null == m_date && !Populate())
                {
                    return null;
                }
                return m_date;
            }
            internal set { m_date = value; }
        }

        protected string m_subject;
        public string Subject
        {
            get
            {
                if (null == m_subject && !Populate())
                {
                    return null;
                }
                return m_subject;
            }
            internal set { m_subject = value; }
        }

        protected string m_body;
        public string Body
        {
            get
            {
                if (null == m_body && !GetBody())
                {
                    return null;
                }
                return m_body;
            }
            internal set { m_body = value; }
        }

        internal static string FormatString { get { return " --pretty=format:\"%H %P\t%an\t%ae\t%ad\t%s\" "; } }

        internal string FullFormatString { get { return " --pretty=format:\"%H %P\t%an\t%ae\t%ad\t%s\" "; } } //use this to get body too
        
        public Commit(string path, string id) : base(path)
        {
            m_hash = id;
        }

        protected Commit(string path) : base(path)
        {
        }

        public static Commit GetCommit(string path, string record)
        {
            Commit c = new Commit(path);
            c.Populate(record);
            return c;
        }

        protected bool GetBody()
        {
            Executioner e = Executioner.GetExecutioner(m_path, true);
            string output, error;
            if (0 == e.Execute("git-show", "--name-only --pretty=format:%b " + ID, "", out output, out error))
            {
                m_body = output.Trim();
                return true;
            }
            return false;
        }

        protected bool Populate()
        {
            Executioner e = Executioner.GetExecutioner(m_path, true);
            string output, error;
            if (0 == e.Execute("git-show", FormatString + ID, "", out output, out error))
            {
                return Populate(output.Trim());
            }
            return false;
        }

        internal bool Populate(string record)
        {
            try
            {
                int idx1 = record.IndexOf(" ");
                string id = record.Substring(0, idx1);
                int idx2 = record.IndexOf("\t", idx1 + 1);
                string[] parents = record.Substring(idx1 + 1, (idx2 - idx1) - 1).Split(" ".ToCharArray());
                idx1 = record.IndexOf("\t", idx2 + 1);
                string authorName = record.Substring(idx2 + 1, (idx1 - idx2) - 1);
                idx2 = record.IndexOf("\t", idx1 + 1);
                string authorEmail = record.Substring(idx1 + 1, (idx2 - idx1) - 1);
                idx1 = record.IndexOf("\t", idx2 + 1);
                string date = record.Substring(idx2 + 1, (idx1 - idx2) - 1);

                string subject = record.Substring(idx1 + 1);
                ID = id;
                Parents = new List<string>(parents);
                AuthorName = authorName;
                AuthorEmail = authorEmail;
                Date = date;
                Subject = subject;
            }
            catch (IndexOutOfRangeException ex)
            {
                System.Console.WriteLine(ex.Message + " " + ex.StackTrace);
                return false;
            }
            return true;
        }
    }
}