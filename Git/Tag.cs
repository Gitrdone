using System;
using System.Collections.Generic;
using System.Text;

namespace Git
{
    public class Tag : Treeish
    {
        protected string m_name;
        public override string Name { get { return m_name; } internal set { m_name = value; }  }
        
        protected string m_hash;
        public override string ID { get { return m_hash; } internal set { m_hash = value; } }
        
        public Tag(string name, string path) : base(path)
        {
            m_name = name;
        }
    }
}
